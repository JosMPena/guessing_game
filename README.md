# Guessing game

* Must have elixir installed in your sistem

1. open `iex` in your terminal/console
2. compile the executable `c "guessing_game.ex"`
3. start the game by asking it to guess between 2 numbers i.e: `GuessingGame.guess(1, 50)`
4. enjoy :)
press `ctrl+c` twice to exit iex
